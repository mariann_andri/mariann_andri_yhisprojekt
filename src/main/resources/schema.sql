-- Siin loome bootrunnimisel loodava andmebaasi struktuuri. Andmed on data.sql-is.

-- Kustutame olemasolevad tabelid. Alustame lastest ja lõpetame vanematega, sest FK piirangud on peal.
DROP TABLE IF EXISTS `taps`;
DROP TABLE IF EXISTS `shipments`;
DROP TABLE IF EXISTS `studies`;

DROP TABLE IF EXISTS `user`;

-- Asume tabeleid looma. Loomisel ta lülitab FK piirangud ajutiselt välja ja pärast loomist paneb sisse.
-- Ära unusta käsu lõpust semikoolonit!
CREATE TABLE IF NOT EXISTS `studies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `study_nr` varchar(50) NOT NULL,
  `client` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Study_nr_UNIQUE` (`study_nr`)
);

CREATE TABLE IF NOT EXISTS `shipments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `study_nr` varchar(50) NOT NULL,
  `shipment_nr` int(11) DEFAULT NULL,
  `order_date` date NOT NULL,
  `number_of_patients` int(11) NOT NULL,
  `number_of_kits` int(11) DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `dispatch_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_shipments_studies` FOREIGN KEY (`study_nr`) REFERENCES `studies` (`study_nr`)
);


CREATE TABLE IF NOT EXISTS `taps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipment_id` int(11) NOT NULL,
  `marking` varchar(45) DEFAULT NULL,
  `combination` varchar(45) DEFAULT NULL,
  `tap_nr` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `taps in shipment_idx` (`shipment_id`),
  CONSTRAINT `taps in shipment` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`id`)
);

CREATE TABLE `user` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(190) NOT NULL,
    `password` VARCHAR(190) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE(username)
);