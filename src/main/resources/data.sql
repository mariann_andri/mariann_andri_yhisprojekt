-- Siin paneme andmed schema.sql-is loodud andmebaasi tabelitesse.
-- Ära unusta käsurea lõpust semikoolonit!

INSERT INTO `studies` (`id`, `study_nr`, `client`) VALUES
	(1, 'FTXT-TEST1', 'ERR'),
	(2, 'FTXT-TEST2', 'TV3');

INSERT INTO `shipments`
(`id`, `study_nr`, `shipment_nr`, `order_date`, `number_of_patients`, `number_of_kits`, `expiry_date`, `dispatch_date`) VALUES
	(1, 'FTXT-TEST1', 1, '2020-01-16', 5, 10, '2021-01-16', '2020-01-17'),
	(2, 'FTXT-TEST1', 2, '2020-01-18', 5, 10, '2021-01-18', '2020-01-19'),
	(3, 'FTXT-TEST2', 1, '2020-01-17', 5, 15, '2021-01-17', '2020-01-18');

INSERT INTO `taps` (`id`, `shipment_id`, `marking`, `combination`, `tap_nr`) VALUES
(1,1,'1','Tap 1 combination','TAP-1'),
(2,1,'1L','Tap 2 combination','TAP-2'),
(3,1,'1NL','Tap 3 combination','TAP-3'),
(4,2,'2','Tap 10 combination','TAP-10'),
(5,2,'2L','Tap 11 combination','TAP-11'),
(6,2,'2NL','Tap 12 combination','TAP-12'),
(7,2,'3','Tap 13 combination','TAP-13'),
(8,2,'3L','Tap 14 combination','TAP-14'),
(9,2,'3NL','Tap 15 combination','TAP-15'),
(10,3,'1','Tap 1 combination','TAP-1'),
(11,3,'1L','Tap 2 combination','TAP-2'),
(12,3,'1NL','Tap 3 combination','TAP-3');

INSERT INTO `user` (username, password) VALUES ('admin', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei');