package com.MariannAndri.MariannAndriProjekt.Service;

import com.MariannAndri.MariannAndriProjekt.Model.User;
import com.MariannAndri.MariannAndriProjekt.Repository.UserRepository;
import com.MariannAndri.MariannAndriProjekt.dto.*;
import io.jsonwebtoken.lang.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenService jwtTokenService;

    @Autowired
    private PasswordEncoder passwordEncoder;


    public GenericResponseDto register(UserRegistrationDto userRegistration) {
        GenericResponseDto responseDto = new GenericResponseDto();
        User user = new User(userRegistration.getUsername(), passwordEncoder.encode(userRegistration.getPassword()));
        if (!userRepository.userExists(userRegistration.getUsername())) {
            userRepository.addUser(user);
        } else {
            responseDto.getErrors().add("User with the specified username already exists.");
        }
        return responseDto;
    }

    public JwtResponseDto authenticate(JwtRequestDto request) throws Exception {
        String error = authenticate(request.getUsername(), request.getPassword());
        if (error == null) {
            final User userDetails = userRepository.getUserByUsername(request.getUsername());
            final String token = jwtTokenService.generateToken(userDetails.getUsername());
            return new JwtResponseDto(userDetails.getId(), userDetails.getUsername(), token, new ArrayList<>());

        } else {
            return new JwtResponseDto(0, null, null, Arrays.asList(error));
        }

    }

    private String authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            return null;
        } catch (DisabledException e) {
            return "USER_DISABLED";
        } catch (BadCredentialsException e) {
            return "Kontrolli parooli!";
        }
    }

    public User getCurrentlyLoggedInUser() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        if (username != null) {
            return userRepository.getUserByUsername(username);
        }
        return null;
    }

    public void editUser(UserEditDto userEdit) {
        Assert.isTrue(userEdit.getPassword().length() >= 2, "Password not long enough!");
        User currentlyLoggedInUser = getCurrentlyLoggedInUser();
        UserEditDto userEditEntityWithCryptedPassword = new UserEditDto(passwordEncoder.encode(userEdit.getPassword()));
        userRepository.updateUser(userEditEntityWithCryptedPassword, currentlyLoggedInUser.getId());
    }
}