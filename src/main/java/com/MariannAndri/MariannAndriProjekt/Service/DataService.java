package com.MariannAndri.MariannAndriProjekt.Service;

import com.MariannAndri.MariannAndriProjekt.Model.Shipments;
import com.MariannAndri.MariannAndriProjekt.Model.Studies;
import com.MariannAndri.MariannAndriProjekt.Model.Tap;
import com.MariannAndri.MariannAndriProjekt.Repository.DataRepository;
import io.jsonwebtoken.lang.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataService {

    @Autowired
    private DataRepository dataRepository;

    public void editStudy(Studies studies) {
        if (studies.getId() > 0) {

            Assert.isTrue(studies.getStudyNumber().length() > 1 && studies.getStudyNumber().length() <= 50, "Uuringu nime pikkus peab olema 2-50 tähemärki!");
            Assert.isTrue(studies.getClient().length() > 1 && studies.getClient().length() <= 100, "Kliendi nime pikkus peab olema 2-100 tähemärki!");

            dataRepository.updateSingleStudy(studies);

        } else {

            Assert.isTrue(studies.getStudyNumber().length() > 1 && studies.getStudyNumber().length() <= 50, "Uuringu nime pikkus peab olema 2-50 tähemärki!");
            Assert.isTrue(studies.getClient().length() > 1 && studies.getClient().length() <= 100, "Kliendi nime pikkus peab olema 2-100 tähemärki!");

            dataRepository.addSingleStudy(studies);
        }
    }

    public void editShipment(Shipments shipments) {
        if (shipments.getId() > 0) {

            Assert.isTrue(shipments.getOrderDate().length() > 0, "Tellimusel peab olema kuupäev!");
            Assert.isTrue(shipments.getNumberOfKits() >= 0, "Komplektide arv ei tohi olla negatiivne!");
            Assert.isTrue(shipments.getNumberOfPatients() >= 0, "Patsientide arv ei tohi olla negatiivne!");

            Assert.isTrue(shipments.getExpiryDate() == null || shipments.getOrderDate().compareTo(shipments.getExpiryDate()) <= 0,
                    "Aegumiskuupäev peab olema pärast tellimuse esitamise kuupäeva!");

            Assert.isTrue(shipments.getDispatchDate() == null || shipments.getOrderDate().compareTo(shipments.getDispatchDate()) <= 0,
                    "Väljasaatmiskuupäev ei saa olla enne tellimuse esitamise kuupäeva!");

            if (shipments.getDispatchDate() != null && shipments.getExpiryDate() != null) {
                Assert.isTrue(shipments.getDispatchDate().compareTo(shipments.getExpiryDate()) <= 0, "Väljasaatmiskuupäev ei saa olla pärast saadetise aegumise kuupäeva!");
            } else if (shipments.getDispatchDate() != null && shipments.getExpiryDate() == null) {
                Assert.isTrue(shipments.getExpiryDate().compareTo(shipments.getDispatchDate()) <= 0, "Saadetise väljasaatmisel peab olema lisatud saadetise aegumise kuupäev!");
            }

            dataRepository.updateSingleShipment(shipments);

        } else {

            Assert.isTrue(shipments.getOrderDate().length() > 0, "Tellimusel peab olema kuupäev!");
            Assert.isTrue(shipments.getNumberOfKits() >= 0, "Komplektide arv ei tohi olla negatiivne!");
            Assert.isTrue(shipments.getNumberOfPatients() >= 0, "Patsientide arv ei tohi olla negatiivne!");

            Assert.isTrue(shipments.getExpiryDate() == null || shipments.getOrderDate().compareTo(shipments.getExpiryDate()) <= 0,
                    "Aegumiskuupäev peab olema pärast tellimuse esitamise kuupäeva!");

            Assert.isTrue(shipments.getDispatchDate() == null || shipments.getOrderDate().compareTo(shipments.getDispatchDate()) <= 0,
                    "Väljasaatmiskuupäev ei saa olla enne tellimuse esitamise kuupäeva!");

            if (shipments.getDispatchDate() != null && shipments.getExpiryDate() != null) {
                Assert.isTrue(shipments.getDispatchDate().compareTo(shipments.getExpiryDate()) <= 0, "Väljasaatmiskuupäev ei saa olla pärast saadetise aegumise kuupäeva!");
            } else if (shipments.getDispatchDate() != null && shipments.getExpiryDate() == null) {
                Assert.isTrue(shipments.getExpiryDate().compareTo(shipments.getDispatchDate()) <= 0, "Saadetise väljasaatmisel peab olema lisatud saadetise aegumise kuupäev!");
            }

            dataRepository.addSingleShipment(shipments);
        }
    }

    public void editTap (Tap tap){
        if(tap.getId()>0){
            dataRepository.updateSingleTap(tap);
        }else {
            dataRepository.addSingleTap(tap);
        }
    }

}
