package com.MariannAndri.MariannAndriProjekt.Service;

import com.MariannAndri.MariannAndriProjekt.Model.Shipments;
import com.MariannAndri.MariannAndriProjekt.Model.Studies;
import com.MariannAndri.MariannAndriProjekt.Model.Tap;
import com.MariannAndri.MariannAndriProjekt.Repository.DataRepository;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.PrimitiveIterator;
import java.util.UUID;

@Service
public class FileService {
    @Value("${file.upload-dir}")
    private String uploadDir;
    @Autowired
    private DataRepository dataRepository;


    private Path getUploadDir() {
        return Paths.get(this.uploadDir).toAbsolutePath().normalize();
    }

    public String storeFile(MultipartFile file) throws IOException {
        String fileName = generateFileName(file);
        Path targetLocation = this.getUploadDir().resolve(fileName);
        Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
        return fileName;
    }

    public Resource loadFileAsResource(String fileName) throws MalformedURLException {
        Path filePath = this.getUploadDir().resolve(fileName).normalize();
        return new UrlResource(filePath.toUri());
    }

    private String generateFileName(MultipartFile inputFile) {
        String fileName = StringUtils.cleanPath(inputFile.getOriginalFilename());
        String extension = "";
        int i = fileName.lastIndexOf(".");
        if (i > 0) {
            extension = fileName.substring(i);
        }
        return UUID.randomUUID().toString() + extension;
    }

//    private String generatePdfFileName() {
//        return UUID.randomUUID().toString() + ".pdf";
//    } // unikaalse  faili nime genereerimine

    private String generatePdfFileName(Studies study, Shipments shipment, String documentName) {
        return study.getStudyNumber()+ "_shipment_" + shipment.getShipmentNumber() + "_" + documentName + ".pdf";
    }


    private static Font normal = FontFactory.getFont(FontFactory.HELVETICA, 12, BaseColor.BLACK);
    private static Font bold = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, BaseColor.BLACK);
    private static Font bigBold = FontFactory.getFont(FontFactory.HELVETICA, 16, Font.BOLD, BaseColor.BLACK);
    private static Font small = FontFactory.getFont(FontFactory.HELVETICA, 8, BaseColor.BLACK);
    private static Font smallBold = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD, BaseColor.BLACK);


    public static final String LOGO = "/Users/Mariann/dev/fibrotx/mariann_andri_yhisprojekt/mariann_andri_yhisprojekt/src/main/resources/images/logo.png";



    //kutsun controlleris välja generateOrdersheetPDF ja annan kaasa shipmentId parameetri
    public Resource generateOrderSheetPdf (int shipmentId) throws IOException, DocumentException {

        Shipments shipment=dataRepository.fetchSingleShipment(shipmentId);
        Studies study = dataRepository.fetchStudyByStudyNr(shipment.getStudyNumber());
        List<Tap> taps = new ArrayList<>(shipment.getTaps());

        //siin saan määrata genereeritava dokumendi suuruse ja marginid
        Document orderSheet = new Document(PageSize.A4,56,56,56,56);
        String pdfFileName = this.generatePdfFileName(study, shipment, "order_sheet");// siia loogilise nime genereerimine
        String pdfFilePath = this.getUploadDir()+"/" + pdfFileName;// uploadDir on mul määratud kuhu salvestab
        PdfWriter.getInstance(orderSheet, new FileOutputStream(pdfFilePath, false)); // false siin peaks tagama, et fail salvestatakse üle, kui on sama nimi

        orderSheet.open();

        //siin lisaks logo
        addLogo(orderSheet);

        addEmptyLine(orderSheet, 1);
        orderSheet.add(new Phrase("TAP ORDER SHEET", bold));
        addEmptyLine(orderSheet, 3);

        addShipmentInfoOnOrder(study, shipment, orderSheet);
        addEmptyLine(orderSheet,1);
        Paragraph p = new Paragraph();
        p.setIndentationLeft(20);
        addTabbedParagraph(p, 100, "Total nr of Kits: ", Integer.toString(shipment.getNumberOfKits()));
        orderSheet.add(p);

        addItemsInfoTable (orderSheet, shipment);

        addTapTable(taps, orderSheet);

        orderSheet.close();
        return this.loadFileAsResource(pdfFileName);
    }

    public Resource generateDispatchNotePdf (int shipmentId) throws IOException, DocumentException {

        Shipments shipment=dataRepository.fetchSingleShipment(shipmentId);
        Studies study = dataRepository.fetchStudyByStudyNr(shipment.getStudyNumber());
        List<Tap> taps = new ArrayList<>(shipment.getTaps());

        //siin saan määrata genereeritava dokumendi suuruse ja marginid
        Document dispatchNote = new Document(PageSize.A4,56,56,56,56);
        String pdfFileName = this.generatePdfFileName(study, shipment, "dispatch_note");// siia loogilise nime genereerimine
        String pdfFilePath = this.getUploadDir()+"/" + pdfFileName;// uploadDir on mul määratud kuhu salvestab
        PdfWriter.getInstance(dispatchNote, new FileOutputStream(pdfFilePath));

        dispatchNote.open();

        addLogo(dispatchNote);
        addEmptyLine(dispatchNote, 1);
        dispatchNote.add(new Phrase("DISPATCH NOTE ", bold));
        addEmptyLine(dispatchNote, 3);

        addShipper(dispatchNote);
        addEmptyLine(dispatchNote, 1);
        addReciever(dispatchNote, study);
        addEmptyLine(dispatchNote, 1);
        addShipementInfoOnDispatchNote(study, shipment, dispatchNote);

        addEmptyLine(dispatchNote, 2);

        dispatchNote.add(new Phrase("Each package contains:", normal));
        addTapTable(taps, dispatchNote);
        dispatchNote.close();
        return this.loadFileAsResource(pdfFileName);
    }



    public Resource generatePouchLabelPdf (int shipmentId) throws FileNotFoundException, MalformedURLException, DocumentException {

        Shipments shipment=dataRepository.fetchSingleShipment(shipmentId);
        Studies study = dataRepository.fetchStudyByStudyNr(shipment.getStudyNumber());
        List<Tap> taps = new ArrayList<>(shipment.getTaps());




            Rectangle pgSize = new Rectangle(295, 210);
            Document pouchLabel = new Document(pgSize, 15, 15, 15, 15);

//            Rectangle pgSize = new Rectangle(280, 195);
//            Document pouchLabel = new Document(pgSize, 0, 0, 0, 0);
            String pdfFileName = this.generatePdfFileName(study, shipment, "pouch_label");// siia loogilise nime genereerimine
            String pdfFilePath = this.getUploadDir() + "/" + pdfFileName;// uploadDir on mul määratud kuhu salvestab
            PdfWriter.getInstance(pouchLabel, new FileOutputStream(pdfFilePath));

            pouchLabel.open();

        for(int i= 0; shipment.getNumberOfKits()>i; i++) {
            addPLTable(pouchLabel, study, shipment, taps);
            addEmptyLine(pouchLabel, 1);
            pouchLabel.add(new Paragraph("I hereby confirm that all procedures have been performed appropriately.", smallBold));
            addEmptyLine(pouchLabel, 1);
            pouchLabel.add(new Paragraph("Date:........................................ Signature:........................................", small));
            addEmptyLine(pouchLabel, 1);
            pouchLabel.add(new Paragraph("This pacage contains:", small));
            pouchLabel.add(new Paragraph(countTaps(shipment) + " TAPs, " + countTaps(shipment) + "ampoules, 1 user manual, 1 TAP carrier", small));
            pouchLabel.add(new Paragraph("Before use: store this Kit +2...+10°C upon arrival", small));
            pouchLabel.add(Chunk.NEXTPAGE);
        }
            pouchLabel.close();

            return this.loadFileAsResource(pdfFileName);

    }


    private static void addPLTable (Document document, Studies study, Shipments shipment, List<Tap> taps) throws DocumentException {
        String orderComposition = "";
        for (int i=0; i<taps.size(); i++){
            if(i<taps.size()-1){
                orderComposition += taps.get(i).getMarking()+ " - ";
            }else {
                orderComposition += taps.get(i).getMarking();
            }
        }
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100);
        float [] colWidths = {30, 70};
        table.setWidths(colWidths);
        addPLTableCell("Study nr: ",table, small);
        addPLTableCell(study.getStudyNumber(),table, smallBold);
        addPLTableCell("Client: ",table, small);
        addPLTableCell(study.getClient(),table, smallBold);
        addPLTableCell("Order composition: ",table, small);
        addPLTableCell(orderComposition,table, smallBold);
        addPLTableCell("Expiry date: ",table, small);
        addPLTableCell(shipment.getExpiryDate(),table, smallBold);

        document.add(table);
    }

    private static void addPLTableCell (String phrase, PdfPTable table, Font font){
        PdfPCell cell = new PdfPCell(new Phrase(phrase, font));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);
    }

    private static void addItemsInfoTable(Document document, Shipments shipment) throws DocumentException {
        PdfPTable table = new PdfPTable(3);
        float [] colWidths = {40, 30, 30};
        table.setWidths(colWidths);
        table.setWidthPercentage(100);
        table.setSpacingBefore(10);
        addItemTableCellLeft("",table);
        addItemTableCellCentre("1 KIT", table);
        addItemTableCellCentre("STUDY TOTAL", table);
        addItemTableCellLeft("Ampoules:",table);
        addItemTableCellCentre(Integer.toString(countTaps(shipment)), table);
        addItemTableCellCentre(Integer.toString(countTaps(shipment) * shipment.getNumberOfKits()), table);
        addItemTableCellLeft("Boxes:",table);
        addItemTableCellCentre(findBoxAndPouch(shipment), table);
        addItemTableCellCentre(Integer.toString(shipment.getNumberOfKits()), table);
        addItemTableCellLeft("User manuals:",table);
        addItemTableCellCentre("1", table);
        addItemTableCellCentre(Integer.toString(shipment.getNumberOfKits()), table);
        addItemTableCellLeft("Pouches:",table);
        addItemTableCellCentre(findBoxAndPouch(shipment), table);
        addItemTableCellCentre(Integer.toString(shipment.getNumberOfKits()), table);
        addItemTableCellLeft("Pouch labels:",table);
        addItemTableCellCentre("1", table);
        addItemTableCellCentre(Integer.toString(shipment.getNumberOfKits()), table);
        document.add(table);
    }

    private static void addItemTableCellCentre(String phrase, PdfPTable table){
        PdfPCell cellValue = new PdfPCell(new Phrase(phrase));
        cellValue.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellValue.setBorderColor(BaseColor.LIGHT_GRAY);
        cellValue.setPadding(10);
        table.addCell(cellValue);
    }
    private static void addItemTableCellLeft(String phrase, PdfPTable table){
        PdfPCell cellValue = new PdfPCell(new Phrase(phrase));
        cellValue.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellValue.setBorderColor(BaseColor.LIGHT_GRAY);
        cellValue.setPadding(10);
        table.addCell(cellValue);
    }


    private static void addShipmentInfoOnOrder (Studies study, Shipments shipment, Document document) throws DocumentException {
        Paragraph p = new Paragraph();
        p.setAlignment(Element.ALIGN_LEFT);
        p.setIndentationLeft(20);
        addTabbedParagraph(p, 100,"Client: ", study.getClient());
        addTabbedParagraph(p, 100,"Study nr: ", shipment.getStudyNumber());
        addTabbedParagraph(p, 100, "Shipment nr: ", Integer.toString(shipment.getShipmentNumber()));
        addTabbedParagraph(p, 100, "Order date: ", shipment.getOrderDate());
        document.add(p);
    }


    private static void addShipementInfoOnDispatchNote (Studies study, Shipments shipment, Document document) throws DocumentException {
        Paragraph p = new Paragraph();
        p.setAlignment(Element.ALIGN_LEFT);
        p.setIndentationLeft(20);
        addTabbedParagraph(p, 190, "Study: ", study.getStudyNumber());
        addTabbedParagraph(p, 190,"Shipment: ", Integer.toString(shipment.getShipmentNumber()));
        addTabbedParagraph(p, 190,"Total number of Tap Kits: ", Integer.toString(shipment.getNumberOfKits()));
        addTabbedParagraph(p, 190,"Expiry date: ", shipment.getExpiryDate());
        addTabbedParagraph(p, 190,"Patient files: ", Integer.toString(shipment.getNumberOfPatients()));
        document.add(p);
    }

    private static void addTabbedParagraph (Paragraph paragraph, int tabInterval, String key, String value) throws DocumentException {
        Phrase p = new Paragraph();
        p.setFont(normal);
        p.setTabSettings(new TabSettings(tabInterval));
        p.add(key);
        p.add(Chunk.TABBING);
        p.add(value);
        paragraph.add(p);
    }

    private static void addTapTable (List <Tap> taps, Document document) throws DocumentException {
        PdfPTable table = new PdfPTable(3);
        float[] colWidths= {20,60,20};
        table.setWidths(colWidths);
        table.setWidthPercentage(100);
        table.setSpacingBefore(20);
        addTapTableCell("TAP MARKING", table);
        addTapTableCell("TAP COMBINATION", table);
        addTapTableCell("TAP NUMBER", table);

        for(int i=0; i<taps.size(); i++){
            addTapTableCell(taps.get(i).getMarking(), table);
            addTapTableCell(taps.get(i).getCombination(), table);
            addTapTableCell(taps.get(i).getTapNr(), table);
        }
        document.add(table);
    }

    private static void addTapTableCell (String phrase, PdfPTable table){
        PdfPCell cellValue = new PdfPCell(new Phrase(phrase));
        cellValue.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellValue.setBorderColor(BaseColor.LIGHT_GRAY);
//        cellValue.setBorder(Rectangle.NO_BORDER);
        cellValue.setPadding(10);
        table.addCell(cellValue);
    }

    private static void addMetaData(Document document){
        document.addTitle("Order Sheet");
        document.addAuthor("FibroTx");
        document.addTitle("Order Sheet");
        document.addSubject("Order sheet");
        document.addKeywords("FibroTx, order, shipment");
        document.addAuthor("FibroTx");
        document.addCreator("FibroTx");
    }

    private static void addEmptyLine(Document document, int number) throws DocumentException {
        for(int i=0; i< number; i++){
            document.add(new Phrase("\n"));
        }
    }
//document.add(new Phrase("\n")) his is much more useful than Chunk.NEWLINE

    private static int countTaps(Shipments shipment){
        List<Tap> taps = new ArrayList<>(shipment.getTaps());
        int tapsCount = taps.size();
        return tapsCount;
    }

    private static String findBoxAndPouch (Shipments shipment){
        int tapsCount = countTaps(shipment);
        if (tapsCount<= 4){
            return "1 small";
        }else{
            return "1 big";
        }
    }

    private static void addLogo (Document document) throws IOException, DocumentException {
        Image img = Image.getInstance(LOGO);
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100);
        float[] colWidths= {75,25};
        table.setWidths(colWidths);

        PdfPCell emptyCell = new PdfPCell(new Phrase( ""));
        emptyCell.setBorder(Rectangle.NO_BORDER);
        table.addCell(emptyCell);

        PdfPCell logoCell = new PdfPCell(img, true);
        logoCell.setBorder(Rectangle.NO_BORDER);
        logoCell.setPadding(10);
        table.addCell(logoCell);

        document.add(table);
    }

    private static void addShipper (Document document) throws DocumentException {
        Paragraph p = new Paragraph();
        p.setTabSettings(new TabSettings(80f));

        Phrase title = new Phrase("SHIPPER:");
        title.setFont(normal);
        p.add(title);
        p.add(Chunk.TABBING);

        Phrase companyName = new Phrase("OÜ Research Company");
        companyName.setFont(bold);
        p.add(companyName);

        p.add(Chunk.NEWLINE);
        p.add(Chunk.TABBING);
        Phrase address = new Phrase("Uuringute Allee 56");
        address.setFont(bold);
        p.add(address);

        p.add(Chunk.NEWLINE);
        p.add(Chunk.TABBING);
        Phrase city = new Phrase("Tallinn 10134");
        city.setFont(bold);
        p.add(city);

        document.add(p);
    }

    private static void addReciever (Document document, Studies study) throws DocumentException {
        Paragraph p = new Paragraph();
        p.setTabSettings(new TabSettings(80f));

        Phrase title = new Phrase("RECIEVER:");
        title.setFont(normal);
        p.add(title);
        p.add(Chunk.TABBING);

        Phrase companyName = new Phrase(study.getClient());
        companyName.setFont(bold);
        p.add(companyName);

        p.add(Chunk.NEWLINE);
        p.add(Chunk.TABBING);
        Phrase address = new Phrase("Tänav 36");
        address.setFont(bold);
        p.add(address);

        p.add(Chunk.NEWLINE);
        p.add(Chunk.TABBING);
        Phrase city = new Phrase("Linn 10111");
        city.setFont(bold);
        p.add(city);

        document.add(p);
    }
//    public static PdfPCell createImageCell (String path) throws IOException, BadElementException {
//        Image img = Image.getInstance(path);
//        PdfPCell cell = new PdfPCell(img, true);
//        return cell;
//    }

    //Rectangle size = new Rectangle(image.getPlainWidth(), image.getPlainHeight());
    //document.setPageSize(size);
    // The size of this page is 216x720 points
    //216pt / 72 points per inch = 3 inch
    // 720pt / 72 points per inch = 10 inch
    //The size of this page is 3x10 inch
    // 3 inch x 2.54 = 7.62 cm
    //10 inch x 2.54 = 25.4 cm
    // The size of this page is 7.62x25.4 cm.

    //siin saan määrata genereeritava dokumendi suuruse ja marginid
//        double widthcm=10.5;
//        double heightcm=7.5;
//        double widthpt=(widthcm * 72)/ 2.54;
//        double heightpt=(heightcm * 72)/ 2.54;;







}
