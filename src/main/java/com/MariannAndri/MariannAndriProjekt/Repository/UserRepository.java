package com.MariannAndri.MariannAndriProjekt.Repository;

import com.MariannAndri.MariannAndriProjekt.Model.User;
import com.MariannAndri.MariannAndriProjekt.dto.UserEditDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;

import java.security.Security;
import java.util.List;

@Repository
public class UserRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public boolean userExists(String username) {
        Integer count = jdbcTemplate.queryForObject(
                "select count(id) from user where username = ?",
                new Object[]{username},
                Integer.class
        );
        return count != null && count > 0;
    }

    public void addUser(User user) {
        jdbcTemplate.update("insert into `user` (`username`, `password`) values (?, ?)", user.getUsername(), user.getPassword());
    }

    public User getUserByUsername(String username) {
        List<User> users = jdbcTemplate.query(
                "select * from `user` where `username` = ?",
                new Object[]{username},
                (rs, rowNum) -> new User(rs.getInt("id"), rs.getString("username"), rs.getString("password"))
        );
        return users.size() > 0 ? users.get(0) : null;
    }

    public void updateUser(UserEditDto userEdit, int userId) {
        jdbcTemplate.update("update user set password = ? where id = ?", userEdit.getPassword(), userId);
    }
}
