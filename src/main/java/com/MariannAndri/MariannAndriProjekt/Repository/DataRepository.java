package com.MariannAndri.MariannAndriProjekt.Repository;

import com.MariannAndri.MariannAndriProjekt.Model.Shipments;
import com.MariannAndri.MariannAndriProjekt.Model.Studies;
import com.MariannAndri.MariannAndriProjekt.Model.Tap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Repository
public class DataRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Studies> fetchAllStudies() {
        return jdbcTemplate.query("Select * from studies;",
                (dbRow, sequenceNumber) -> {
                    return new Studies(
                            dbRow.getInt("id"),
                            dbRow.getString("study_nr"),
                            dbRow.getString("client"));

                });
    }
    public Studies fetchSingleStudy (int studyId) {
        List <Studies> studies = jdbcTemplate.query(
                "select * from studies where id = ?;",
                new Object[] {studyId},
                (dbRow, sequneceNumber) -> {
                    return new Studies(
                            dbRow.getInt("id"),
                            dbRow.getString("study_nr"),
                            dbRow.getString("client"));
                });
        if (studies.size()>0){
            return studies.get(0);
        } else {
            return null;
        }
    }


//      adding study
    public void addSingleStudy(Studies studies){
        jdbcTemplate.update("insert into studies (`study_nr`, `client`) values (?, ?)", studies.getStudyNumber(), studies.getClient());
    }

//      updating study
    public void updateSingleStudy(Studies studies){
        jdbcTemplate.update("update studies set `study_nr` = ?, `client` = ? where `id` = ? ", studies.getStudyNumber(), studies.getClient(), studies.getId()
        );
    }

//      removing study
    public void deleteSingleStudy(String studyNumber){
        jdbcTemplate.update("delete from studies where `study_nr` = ?", studyNumber);
    }

    //      removing study
    public void deleteSingleStudyById(int id){
        jdbcTemplate.update("delete from studies where `id` = ?", id);
    }



    //SHIPMENTS

//    adding shipment
    public void addSingleShipment(Shipments shipment){
        jdbcTemplate.update("insert into shipments " +
                "(`study_nr`, `shipment_nr`, `order_date`, `number_of_patients`, `number_of_kits`, `expiry_date`, `dispatch_date`)" +
                " values (?, ?, ?, ?, ?, ?, ?)",
                shipment.getStudyNumber(), shipment.getShipmentNumber(), shipment.getOrderDate(),
                shipment.getNumberOfPatients(), shipment.getNumberOfKits(), shipment.getExpiryDate(), shipment.getDispatchDate());
    }

//    updating shipment
    public void updateSingleShipment(Shipments shipment) {
        jdbcTemplate.update("update shipments set `study_nr` = ?, `shipment_nr` = ?, `order_date` = ?," +
                        " `number_of_patients` = ?, `number_of_kits` = ?, `expiry_date` = ?, `dispatch_date` = ? where id = ? ",
                shipment.getStudyNumber(), shipment.getShipmentNumber(), shipment.getOrderDate(), shipment.getNumberOfPatients(),
                shipment.getNumberOfKits(), shipment.getExpiryDate(), shipment.getDispatchDate(), shipment.getId()
                );
    }
//    deleting shipment
    public void deleteSingleShipment(int shipmentId) {
        jdbcTemplate.update("delete from shipments where `id` = ?", shipmentId);
    }
// see tagastab mitmes sihpment sellel uuringul on
    public int countShipments (String studyNumber){
        return jdbcTemplate.queryForObject("select count(*) from shipments where study_nr=? ",
                new Object[] {studyNumber}, Integer.class);
    }

    //TAPS

    //      adding taps
    public void addSingleTap(Tap tap){
        jdbcTemplate.update("insert into taps (`shipment_id`, `marking`, `combination`, `tap_nr`) values (?, ?, ?, ?)",
                tap.getShipmentId(), tap.getMarking(), tap.getCombination(), tap.getTapNr());
    }

    //      updating taps
    public void updateSingleTap(Tap tap){
        jdbcTemplate.update("update taps set `shipment_id`= ?, `marking`= ?, `combination` =?, `tap_nr`= ? where id =?",
                tap.getShipmentId(), tap.getMarking(), tap.getCombination(), tap.getTapNr(), tap.getId());
    }

    //      removing taps
    public void deleteSingleTap(int id){
        jdbcTemplate.update("delete from taps where `id` = ?", id);
    }



    public Tap fetchSingleTap (int id) {
        List <Tap> tap = jdbcTemplate.query(
                "select * from taps where `id` = ?;",
                new Object[] {id},
                (dbRow, sequneceNumber) -> {
                    return new Tap(
                            dbRow.getInt("id"),
                            dbRow.getInt("shipment_id"),
                            dbRow.getString("marking"),
                            dbRow.getString("combination"),
                            dbRow.getString("tap_nr"));
                });
        if (tap.size()>0){
            return tap.get(0);
        } else {
            return null;
        }
    }





    public List<Studies> fetchClientStudies (String  client) {
        List <Studies> studies = jdbcTemplate.query(
                "select * from studies where client = ?;",
                new Object[] {client},
                (dbRow, sequneceNumber) -> {
                    return new Studies(
                            dbRow.getInt("id"),
                            dbRow.getString("study_nr"),
                            dbRow.getString("client"));
                });
        if (studies.size()>0){
            return studies;
        } else {
            return null;
        }
    }

    public List<Shipments> fetchAllShipments() {
        return jdbcTemplate.query("SELECT * FROM shipments;",
                (dbRow, sequenceNumber) -> {
                    return new Shipments(
                            dbRow.getInt("id"),
                            dbRow.getString("study_nr"),
                            dbRow.getInt("shipment_nr"),
                            dbRow.getString("order_date"),
                            dbRow.getInt("number_of_patients"),
                            dbRow.getInt("number_of_kits"),
                            dbRow.getString("expiry_date"),
                            dbRow.getString("dispatch_date")
                    );
                });
    }

    public Shipments fetchSingleShipment (int id){
        List <Shipments> shipment = jdbcTemplate.query(
                "select * from shipments where id =?;",
                 new Object[] {id},
                (dbRow, sequenceNumber) -> {
            return new Shipments(
                    dbRow.getInt("id"),
                    dbRow.getString("study_nr"),
                    dbRow.getInt("shipment_nr"),
                    dbRow.getString("order_date"),
                    dbRow.getInt("number_of_patients"),
                    dbRow.getInt("number_of_kits"),
                    dbRow.getString("expiry_date"),
                    dbRow.getString("dispatch_date"),
                    this.fetchTaps (dbRow.getInt("id"))
            );
        });
        if (shipment.size()>0){
            return shipment.get(0);
        } else {
            return null;
        }
    }

//    public List<Shipments> fetchClientShipment(@PathVariable String client) {
//        return jdbcTemplate.query("SELECT sh.* FROM shipments sh INNER JOIN studies st ON sh.study_nr = st.study_nr where st.client = ?;",
//                new Object[] {client},
//                (dbRow, sequenceNumber) -> {
//                    return new Shipments(
//                            dbRow.getInt("id"),
//                            dbRow.getString("study_nr"),
//                            dbRow.getInt("shipment_nr"),
//                            dbRow.getString("order_date"),
//                            dbRow.getInt("number_of_patients"),
//                            dbRow.getInt("number_of_kits"),
//                            dbRow.getString("expiry_date"),
//                            dbRow.getString("dispatch_date"));
//                });
//    }

    //uuringud ja nende shipmendid ja nende tap-id
    public List<Studies> fetchStudiesWithDetails () {
        return jdbcTemplate.query("select * from studies order by id desc",
                (dbRow, sequenceNr) -> {
                    return new Studies(
                            dbRow.getInt("id"),
                            dbRow.getString("study_nr"),
                            dbRow.getString("client"),
                            this.fetchShipments(dbRow.getString("study_nr"))
                    );
                });
    }

    //üks uuring study tabeli kliendi veeru
    public List <Studies> fetchStudyWithDetailsByClient (String client) {
        return jdbcTemplate.query(
                "select * from studies where client like ? order by id desc;",
                new Object[] {"%" +client+ "%"},
                (dbRow, sequenceNr) -> {
                    return new Studies(
                            dbRow.getInt("id"),
                            dbRow.getString("study_nr"),
                            dbRow.getString("client"),
                            this.fetchShipments(dbRow.getString("study_nr")));
                });

    }

    public Studies fetchStudyByStudyNr (String studyNumber) {
        List <Studies> studies = jdbcTemplate.query(
                "select * from studies where study_nr = ?;",
                new Object[] {studyNumber},
                (dbRow, sequneceNumber) -> {
                    return new Studies(
                            dbRow.getInt("id"),
                            dbRow.getString("study_nr"),
                            dbRow.getString("client"),
                            this.fetchShipments(dbRow.getString("study_nr"))
                    );
                });
        if (studies.size()>0){
            return studies.get(0);
        } else {
            return null;
        }
    }
    //üks uuring study tabeli id veeru järgi
    public List <Studies> fetchStudyWithDetails (int studyId) {
        return jdbcTemplate.query(
                "select * from studies where id = ?;",
                new Object[] {studyId},
                (dbRow, sequenceNr) -> {
                    return new Studies(
                            dbRow.getInt("id"),
                            dbRow.getString("study_nr"),
                            dbRow.getString("client"),
                            this.fetchShipments(dbRow.getString("study_nr"))
                    );
                });

    }


    //abimeetod fetchStudyShipments saadetiste kuvamiseks
    public List<Shipments> fetchShipments (String studyNr){
        return jdbcTemplate.query("select * from shipments where study_nr=? order by id desc",
                new Object[] {studyNr},
                (row, number) -> {
            return new Shipments(
                    row.getInt("id"),
                    row.getString("study_nr"),
                    row.getInt("shipment_nr"),
                    row.getString("order_date"),
                    row.getInt("number_of_patients"),
                    row.getInt("number_of_kits"),
                    row.getString("expiry_date"),
                    row.getString("dispatch_date"),
                    this.fetchTaps (row.getInt("id"))
            );
        });
    }

    //abimeetod fetchStudyShipments tap-ide kuvamiseks
    //See töötab
    public List<Tap> fetchTaps (int shipmentId){
        return jdbcTemplate.query("SELECT * FROM taps where shipment_id = ?",
                new Object[] {shipmentId},
                (row, number) -> {
                    return new Tap(
                            row.getInt("id"),
                            row.getInt("shipment_id"),
                            row.getString("marking"),
                            row.getString("combination"),
                            row.getString("tap_nr")
                    );
                });
    }

}
