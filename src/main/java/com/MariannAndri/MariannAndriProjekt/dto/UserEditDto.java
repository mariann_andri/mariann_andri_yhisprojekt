package com.MariannAndri.MariannAndriProjekt.dto;

public class UserEditDto {
    private String password;

    public UserEditDto(String password) {
        this.password = password;
    }

    public UserEditDto() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
