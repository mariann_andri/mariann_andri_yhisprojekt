package com.MariannAndri.MariannAndriProjekt.dto;

import java.util.ArrayList;
import java.util.List;

public class JwtResponseDto {

    private int id;
    private String username;
    private String token;
    private List<String> errors = new ArrayList<>();


    public JwtResponseDto(int id, String username, String token, List<String> errors) {
        this.id = id;
        this.username = username;
        this.token = token;
        this.errors = errors;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getToken() {
        return this.token;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
