package com.MariannAndri.MariannAndriProjekt.Model;

public class Tap {
    private int id;
    private int shipmentId;
    private String marking;
    private String combination;
    private String tapNr;

    public Tap(int id, int shipmentId, String marking, String combination, String tapNr) {
        this.id = id;
        this.shipmentId = shipmentId;
        this.marking = marking;
        this.combination = combination;
        this.tapNr = tapNr;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(int shipmentId) {
        this.shipmentId = shipmentId;
    }

    public String getMarking() {
        return marking;
    }

    public void setMarking(String marking) {
        this.marking = marking;
    }

    public String getCombination() {
        return combination;
    }

    public void setCombination(String combination) {
        this.combination = combination;
    }

    public String getTapNr() {
        return tapNr;
    }

    public void setTapNr(String tapNr) {
        this.tapNr = tapNr;
    }
}


