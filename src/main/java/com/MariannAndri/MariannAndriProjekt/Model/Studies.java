package com.MariannAndri.MariannAndriProjekt.Model;

import java.util.List;

public class Studies {
    private int id;
    private String studyNumber;
    private String client;
    private List<Shipments> shipments;
//    Siia võiks tulla ka kliendi kontaktandmed.


    public List<Shipments> getShipments() {
        return shipments;
    }

    public void setShipments(List<Shipments> shipments) {
        this.shipments = shipments;
    }
    public Studies() {

    }

    public Studies(int id, String studyNumber, String client, List<Shipments> shipments) {
        this.id = id;
        this.studyNumber = studyNumber;
        this.client = client;
        this.shipments = shipments;
    }

    public Studies(int id, String studyNumber, String client) {
        this.id = id;
        this.studyNumber = studyNumber;
        this.client = client;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudyNumber() {
        return studyNumber;
    }

    public void setStudyNumber(String studyNumber) {
        this.studyNumber = studyNumber;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

}
