package com.MariannAndri.MariannAndriProjekt.Model;

import java.util.Date;
import java.util.List;

public class Shipments {
    private int id;
    private String studyNumber;
    private int shipmentNumber;
    private String orderDate;
    private int numberOfPatients;
    private int numberOfKits;
    private String expiryDate;
    private String dispatchDate;
    private List<Tap> taps;

//orderSheet-ile vajaliku infoga constructor
    public Shipments(int shipmentNumber, String orderDate, int numberOfPatients, int numberOfKits, String expiryDate, String dispatchDate, List<Tap> taps) {
        this.shipmentNumber = shipmentNumber;
        this.orderDate = orderDate;
        this.numberOfPatients = numberOfPatients;
        this.numberOfKits = numberOfKits;
        this.expiryDate = expiryDate;
        this.dispatchDate = dispatchDate;
        this.taps = taps;
    }

    public List<Tap> getTaps() {
        return taps;
    }


    public void setTaps(List<Tap> taps) {
        this.taps = taps;
    }

    public Shipments() {
    }

    public Shipments(int id, String studyNumber, int shipmentNumber, String orderDate, int numberOfPatients, int numberOfKits, String expiryDate, String dispatchDate, List<Tap> taps) {
        this.id = id;
        this.studyNumber = studyNumber;
        this.shipmentNumber = shipmentNumber;
        this.orderDate = orderDate;
        this.numberOfPatients = numberOfPatients;
        this.numberOfKits = numberOfKits;
        this.expiryDate = expiryDate;
        this.dispatchDate = dispatchDate;
        this.taps = taps;
    }

    public Shipments(int id, String studyNumber, int shipmentNumber, String orderDate, int numberOfPatients, int numberOfKits, String expiryDate, String dispatchDate) {
        this.id = id;
        this.studyNumber = studyNumber;
        this.shipmentNumber = shipmentNumber;
        this.orderDate = orderDate;
        this.numberOfPatients = numberOfPatients;
        this.numberOfKits = numberOfKits;
        this.expiryDate = expiryDate;
        this.dispatchDate = dispatchDate;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudyNumber() {
        return studyNumber;
    }

    public void setStudyNumber(String studyNumber) {
        this.studyNumber = studyNumber;
    }

    public int getShipmentNumber() {
        return shipmentNumber;
    }

    public void setShipmentNumber(int shipmentNumber) {
        this.shipmentNumber = shipmentNumber;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public int getNumberOfPatients() {
        return numberOfPatients;
    }

    public void setNumberOfPatients(int numberOfPatients) {
        this.numberOfPatients = numberOfPatients;
    }

    public int getNumberOfKits() {
        return numberOfKits;
    }

    public void setNumberOfKits(int numberOfKits) {
        this.numberOfKits = numberOfKits;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getDispatchDate() {
        return dispatchDate;
    }

    public void setDispatchDate(String dispatchDate) {
        this.dispatchDate = dispatchDate;
    }
}