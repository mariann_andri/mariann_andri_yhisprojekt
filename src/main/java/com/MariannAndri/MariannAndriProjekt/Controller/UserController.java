package com.MariannAndri.MariannAndriProjekt.Controller;

import com.MariannAndri.MariannAndriProjekt.Service.UserService;
import com.MariannAndri.MariannAndriProjekt.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public GenericResponseDto register(@RequestBody UserRegistrationDto userRegistration) {
        return userService.register(userRegistration);
    }

    @PostMapping("/login")
    public JwtResponseDto authenticate(@RequestBody JwtRequestDto request) throws Exception {
        return userService.authenticate(request);
    }

    @PostMapping("/edit")
    public void editUser(@RequestBody UserEditDto userEdit) {
        userService.editUser(userEdit);
    }
}
