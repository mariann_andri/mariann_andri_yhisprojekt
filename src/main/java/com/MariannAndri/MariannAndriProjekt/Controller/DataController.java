package com.MariannAndri.MariannAndriProjekt.Controller;

import com.MariannAndri.MariannAndriProjekt.Model.Shipments;
import com.MariannAndri.MariannAndriProjekt.Model.Studies;
import com.MariannAndri.MariannAndriProjekt.Model.Tap;
import com.MariannAndri.MariannAndriProjekt.Repository.DataRepository;
import com.MariannAndri.MariannAndriProjekt.Service.DataService;
import com.MariannAndri.MariannAndriProjekt.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/data")
@CrossOrigin("*")
public class DataController {

    @Autowired
    private DataRepository dataRepository;

    @Autowired
    private DataService dataService;


//    Küsime välja kliendi koos uuringu numbriga.
    @GetMapping("/studies")
    public List<Studies> getStudies(){
        return dataRepository.fetchAllStudies();
    }

//    Küsime välja kõik saadetised.
    @GetMapping("/shipments")
    public List<Shipments> getShipments() { return dataRepository.fetchAllShipments();}


////    Küsime saadetisi kliendi nime järgi.
//    @GetMapping("/shipments/{client}")
//    public List<Shipments> getClientNameShipment(@PathVariable String client) {
//        return dataRepository.fetchClientShipment(client);
//    }
// küsime ühte studyt - ID järgi
    @GetMapping ("/study/{studyId}")
    public Studies getSingleStudy (@PathVariable int studyId) {
        return dataRepository.fetchSingleStudy(studyId);
    }

    @GetMapping ("/studybystudynumber/{studyNr}")
    public Studies getSingleStudyByStudyNr (@PathVariable String studyNr) {
        return dataRepository.fetchStudyByStudyNr(studyNr);
    }

    // küsime ühte studyt - kliendi järgi
    @GetMapping ("/studies/{client}")
    public List<Studies> getClientStudies (@PathVariable String client) {
        return dataRepository.fetchClientStudies(client);
    }
    @GetMapping("tap/{id}")
    public Tap getTaps (@PathVariable int id){
        return dataRepository.fetchSingleTap(id);
    }

    // shipment shipment id numbri järgi
    @GetMapping ("/shipment/{id}")
    public Shipments getSingleShipment (@PathVariable int id){
        return dataRepository.fetchSingleShipment(id);
    }

    // study kustutamine
    @DeleteMapping("/deletestudy/{studyNumber}")
    public void deleteStudy(@PathVariable String studyNumber) {
        dataRepository.deleteSingleStudy(studyNumber);
    }

    // study kustutamine
    @DeleteMapping("/deletestudyById/{id}")
    public void deleteStudy(@PathVariable int id) {
        dataRepository.deleteSingleStudyById(id);
    }

    // shipmendi kustutamine
    @DeleteMapping("/deleteshipment/{shipmentId}")
    public void deleteShipment(@PathVariable int shipmentId) {
        dataRepository.deleteSingleShipment(shipmentId);
    }

    @DeleteMapping("deletetap/{id}")
    public void deleteTap (@PathVariable int id) { dataRepository.deleteSingleTap(id);}

    @PostMapping("/editStudy")
    public void editStudy (@RequestBody Studies studies){
        dataService.editStudy(studies);
    }

    @PostMapping("/editShipment")
    public void editShipment(@RequestBody Shipments shipment){
        dataService.editShipment(shipment);
    }

    @PostMapping("/editTap")
    public void editTap (@RequestBody Tap tap) { dataService.editTap(tap);}

    // küsime kõik uuringud ja nende alla kuuluvad saadetised
    @GetMapping ("/saadetised")
    public List<Studies> getStudiesWithDetails () {
        return dataRepository.fetchStudiesWithDetails();
    }
    //üks uuring ja selle saadetised

    // küsime uuringu ja selle alla kuuluvad saadetised
    @GetMapping ("/saadetised/{studyId}")
    public List<Studies> getStudyWithDetails (@PathVariable int studyId) {
        return (List<Studies>) dataRepository.fetchStudyWithDetails(studyId);}

    @GetMapping ("/kliendi_saadetised/{client}")
    public List<Studies> getStudyWithDetailsByClient (@PathVariable String client) {
        return (List<Studies>) dataRepository.fetchStudyWithDetailsByClient(client);}

    @GetMapping ("/countshipments/{studyNumber}")
    public  int countShipments (@PathVariable String studyNumber){
        return dataRepository.countShipments(studyNumber);
    }

}

